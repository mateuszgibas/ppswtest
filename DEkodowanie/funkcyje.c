#include "string.h"

#define NULL 0
#define MAX_TOKEN_NR 3
#define MAX_KEYWORD_STRING_LTH 10
#define MAX_KEYWORD_NR 3

enum KeywordCode
{
    LD,
    ST,
    RST
};

union TokenValue
{
    enum KeywordCode eKeyword;
    unsigned int uiNumber;
    char *pcString;
};

enum TokenType
{
    KEYWORD,
    NUMBER,
    STRING
};

struct Token
{
    enum TokenType eType;
    union TokenValue uValue;
};

struct Token asToken[MAX_TOKEN_NR];


struct Keyword
{
    enum KeywordCode eCode;
    char cString[MAX_KEYWORD_STRING_LTH + 1];
};

struct Keyword asKeywordList[MAX_KEYWORD_NR] = {
    {RST, "reset"},
    {LD, "load"},
    {ST, "store"}
    };

unsigned char ucTokenNr;

enum State
{
    TOKEN,
    DELIMITER
};

unsigned char ucFindTokensInString(char *pcString)
{
    unsigned char ucCharacterCounter;
    unsigned char ucTokenNr = 0;
    unsigned char ucCurrentCharacter;
    enum State eState = DELIMITER;

    for (ucCharacterCounter = 0;; ucCharacterCounter++)
    {
        ucCurrentCharacter = pcString[ucCharacterCounter];
        switch (eState)
        {
        case TOKEN:
        {
            if (ucTokenNr == MAX_TOKEN_NR)
            {
                return ucTokenNr;
            }
            else if (ucCurrentCharacter == NULL)
            {
                return ucTokenNr;
            }
            else if (ucCurrentCharacter != ' ')
            {
                eState = TOKEN;
            }
            else
            {
                eState = DELIMITER;
            }
            break;
        }
        case DELIMITER:
        {
            if (ucCurrentCharacter == NULL)
            {
                return ucTokenNr;
            }
            else if (ucCurrentCharacter == ' ')
            {
                eState = DELIMITER;
            }
            else
            {
                eState = TOKEN;
                asToken[ucTokenNr].uValue.pcString = pcString + ucCharacterCounter;
                ucTokenNr++;
            }
            break;
        }
        }
    }
}



enum Result eStringToKeyword(char pcStr[], enum KeywordCode *peKeywordCode)
{
    unsigned char ucKeywordCounter;
    for (ucKeywordCounter = 0; ucKeywordCounter < MAX_KEYWORD_NR; ucKeywordCounter++)
    {
        if (EQUAL == eCompareString(asKeywordList[ucKeywordCounter].cString, pcStr))
        {
            *peKeywordCode = asKeywordList[ucKeywordCounter].eCode;
            return OK;
        }
    }
    return ERROR;
}


void DecodeTokens(void)
{
    unsigned char ucTokenCounter;
    struct Token *psCurrentToken;
    unsigned int uiTokenValue;
    enum KeywordCode eTokenCode;
    for (ucTokenCounter = 0; ucTokenCounter < ucTokenNr; ucTokenCounter++)
    {
        psCurrentToken = &asToken[ucTokenCounter];
        if (OK == eHexStringToUInt(psCurrentToken->uValue.pcString, &uiTokenValue))
        {
            psCurrentToken->eType = NUMBER;
            psCurrentToken->uValue.uiNumber = uiTokenValue;
        }
        else if (OK == eStringToKeyword(psCurrentToken->uValue.pcString, &eTokenCode))
        {
            psCurrentToken->eType = KEYWORD;
            psCurrentToken->uValue.eKeyword = eTokenCode;
        }
        else
        {
            psCurrentToken->eType = STRING;
        }
    }
}

void DecodeMsg(char *pcString)
{
    ucTokenNr = ucFindTokensInString(pcString);
    ReplaceCharactersInString(pcString, ' ', NULL);
    DecodeTokens();
}

int main(){
	enum Result eWynikError; 
	enum Result eWynikOk;
	enum KeywordCode eTokenCode;
	unsigned char pcSource[]="0x3343 ABDCDEFGH reset";
	char pcToDecode[]="0x3343 ABDCDEFGH reset";
	
//ucFindTokensInString--------------------------------------------------------------------------------------(do watch dodac ucTokenNr i asToken)
	//max liczba tokenow
	ucTokenNr=ucFindTokensInString("0x3343 ABDCDEFGH reset");
	//same delimitery
	ucTokenNr=ucFindTokensInString("    ");
	//delimitery na poczatku stringu, mniej tokenow niz maks
	ucTokenNr=ucFindTokensInString("   0x3343 ABDCDEFGH ");
	//kilka delimiterow pomiedzy tokenami
	ucTokenNr=ucFindTokensInString("0x3343   ABDCDEFGH      reset");
	//za duzo tokenow, (zwraca 3 i ustawia wskazniki na pierwsze 3)
	ucTokenNr=ucFindTokensInString("0x3343 ABDC DEFGH reset");	
//eStringToKeyword----------------------------------------------------------------------(do watcha eWynikError, eWynikOk i eTokenCode)
  //brak slowa kluczowego
  eWynikError = eStringToKeyword("jakis", &eTokenCode);
  //slowo kluczowe
	eWynikOk = eStringToKeyword("reset", &eTokenCode);
//DecodeTokens-----------------------------------------------------------------------------------------(do watcha asToken)
  //liczba i zapis jej, string i ustawienie wskaznika, keyword i zapis kodu
  ucTokenNr=ucFindTokensInString(pcSource);
	ReplaceCharactersInString(pcSource,' ',NULL);
	DecodeTokens();	
  while(0);
//DecodeMsg-------------------------------------------------------------------(do watcha asToken)		
	//liczba, string, keyword
	DecodeMsg(pcToDecode);
	while(0);
}

