#include "string.h"

#define NULL  0

void CopyString(char pcSource[], char pcDestination[]){
	unsigned char ucCounter;

	for(ucCounter=0; pcSource[ucCounter] != NULL; ucCounter++){
		pcDestination[ucCounter] = pcSource[ucCounter];		
	}
	pcDestination[ucCounter] = pcSource[ucCounter];
	return;
}

enum CompResult eCompareString(char pcStr1[], char pcStr2[]){
	unsigned char ucCounter;
	for(ucCounter = 0; pcStr1[ucCounter] != NULL; ucCounter++){
		if( pcStr1[ucCounter] != pcStr2[ucCounter]){
			return DIFFERENT;			
		}
	}
	if( pcStr1[ucCounter] != pcStr2[ucCounter]){
			return DIFFERENT;			
		}
  return EQUAL;		
}

void AppendString(char pcSourceStr[],char pcDestinationStr[]){
	unsigned char ucCounter;
	for(ucCounter = 0; pcDestinationStr[ucCounter] != NULL; ucCounter++){
		continue;
	}
	CopyString(pcSourceStr, pcDestinationStr + ucCounter);
}

void ReplaceCharactersInString(char pcString[],char cOldChar,char cNewChar){
	unsigned char ucCounter;
	for(ucCounter = 0; pcString[ucCounter] != NULL; ucCounter++){
		if( pcString[ucCounter] == cOldChar){		
     	pcString[ucCounter] = cNewChar;
     }
  }
}

void UIntToHexStr(unsigned int uiValue, char pcStr[]){
  unsigned char ucTetradaCounter;
	unsigned char ucCurrentTetrada;
	pcStr[0] = '0';
	pcStr[1] = 'x';
	pcStr[6] = NULL;
	for(ucTetradaCounter = 0; ucTetradaCounter < 4; ucTetradaCounter++){
	  ucCurrentTetrada = (uiValue >> (ucTetradaCounter * 4)) & 0x0F;
		if(ucCurrentTetrada < 10){
			pcStr[5 - ucTetradaCounter] = '0' + ucCurrentTetrada;
		}
		else{
			pcStr[5 - ucTetradaCounter] = 'A' + (ucCurrentTetrada - 10);
    }
  }	
}



enum Result eHexStringToUInt(char pcStr[], unsigned int *puiValue){
	unsigned char ucCharacterCounter;
	unsigned char ucCurrentCharacter;
	*puiValue = 0;
	if((pcStr[0] != '0') || (pcStr[1] != 'x') || (pcStr[2] == NULL)){
		return ERROR;
	}
	for(ucCharacterCounter = 2; ucCharacterCounter < 7; ucCharacterCounter++){
		ucCurrentCharacter = pcStr[ucCharacterCounter];
		if(ucCurrentCharacter == NULL){
			return OK;
		}
		else if(ucCharacterCounter == 6){
			return ERROR;
		}
	  *puiValue = *puiValue << 4;
		if((ucCurrentCharacter <= '9') &&(ucCurrentCharacter >= '0')){
      ucCurrentCharacter = ucCurrentCharacter - '0';
		}			
		else if((ucCurrentCharacter <= 'F') && (ucCurrentCharacter >= 'A')){
			ucCurrentCharacter = ucCurrentCharacter - 'A' + 10;
		     }
		     else{
					 return ERROR;
				 }
		*puiValue = *puiValue | ucCurrentCharacter;
	}
	return ERROR;
}


void AppendUIntToString (unsigned int uiValue, char pcDestinationStr[]){
  unsigned char ucPositionCounter;
	for(ucPositionCounter = 0; pcDestinationStr[ucPositionCounter] != NULL; ucPositionCounter++){
	  continue;
  }
	UIntToHexStr(uiValue, pcDestinationStr + ucPositionCounter);	
}
